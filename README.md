# vike-tsconfigs

TSConfig files for projects created with [create-vike](https://www.npmjs.com/package/create-vike) and [Bâti](https://batijs.dev/).

- [Source code](https://gitlab.com/joaommpalmeiro/vike-tsconfigs)
- [npm package](https://www.npmjs.com/package/vike-tsconfigs)
- [Licenses](https://licenses.dev/npm/vike-tsconfigs/0.1.0)
- [Package Phobia](https://packagephobia.com/result?p=vike-tsconfigs@0.1.0)
- [npm trends](https://npmtrends.com/vike-tsconfigs)

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

Delete the following [top-level options](https://www.typescriptlang.org/tsconfig#extends) (if necessary):

- `"files"`
- `"include"`
- `"exclude"`
- `"references"`

```bash
npm run lint
```

```bash
npm run format
```

## Deployment

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the version in the `Licenses` and `Package Phobia` links at the top.

```bash
echo "v$(npm pkg get version | tr -d \")" | pbcopy
```

- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/vike-tsconfigs/-/tags)

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/vike-tsconfigs).
