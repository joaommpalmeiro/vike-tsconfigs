# Notes

- https://github.com/joaopalmeiro/solidjs-templates-tsconfigs
- https://github.com/dprint/dprint/releases/tag/0.45.1
- https://github.com/vikejs/vike/blob/main/boilerplates/package.json
- https://mariusschulz.com/blog/the-showconfig-compiler-option-in-typescript
- If necessary, update the Personal Access Token for GitLab used in GitHub Desktop from the [Keychain Access](https://support.apple.com/en-gb/guide/keychain-access/kyca1083/mac) app.

## Commands

```bash
npm install -D \
dprint \
npm-run-all2 \
sort-package-json \
typescript
```

```bash
npm init @batijs/app template-bati-react -- --react
```

```bash
rm -rf template-bati-react/ && npm init @batijs/app template-bati-react -- --react && mkdir -p bati-react && npx tsc --project ./template-bati-react/tsconfig.json --showConfig > ./bati-react/tsconfig.json
```
